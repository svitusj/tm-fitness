# README - Topic Modeling as fitness function for Feature Location in Software Models#

Repository containing: 
1) the results of locating features in software models (generation and interpreted models) using an Evolutionary Algorithm with Topic Modeling as fitness function, and 2) the source code of the following: 

* Baseline: LSI fitness.
* Our approach: Topic Modeling fitness (using LDA).
* Random Search.