package usj.svit.approach.flimea.core.operator.mutation;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.ICountableOperation;
import usj.svit.architecture.individual.IIndividualTextEMF;

public interface IMutation<T extends IIndividualTextEMF> extends ICountableOperation {

	/**
	 * mutate all elements from the offspring list
	 * @param config the offspring will be modified!
	 */
	public void mutate(EAConfigTextEMF config);
	
	/**
	 * mutates the element provided as input without checking any probability
	 * By default will flip a single bit selected randomly
	 * @param individual the individual will be modified!
	 */
	public void forceMutation(T individual);
	
	/**
	 * return the number of times that the mutation operation has been executed since the last reset and resets the value to 0.
	 * @return
	 */
	public long getAndResetCalls();
	
	/**
	 * return the number of individuals mutated since the last reset and resets the value to 0.
	 * A call to mutate can affect from 1 to MAX_POPULATION elements.
	 * @return
	 */
	public long getAndResetAffected();
}
