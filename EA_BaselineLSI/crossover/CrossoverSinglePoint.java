package usj.svit.approach.flimea.core.operator.crossover;

import java.util.ArrayList;
import java.util.List;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.Chromosome;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.utils.MathUtils;

public class CrossoverSinglePoint extends BaseOperation implements ICrossover<IIndividualTextEMF>  {
	
	@Override
	public List<IIndividualTextEMF> crossover(EAConfigTextEMF context) {
		
		IIndividualTextEMF f1 = context.getOffSpring().get(0);
		IIndividualTextEMF f2 = context.getOffSpring().get(1);
		
		context.getOffSpring().clear();
		
		List<IIndividualTextEMF> result = new ArrayList<IIndividualTextEMF>();
		
		//no crossover
		if(MathUtils.getRandomDouble() > context.getCrossoverProbability()){
			result.add(new IndividualTextEMF(f1.getCopyOfGenes(),f1.getParent()));
			result.add(new IndividualTextEMF(f2.getCopyOfGenes(),f2.getParent()));
		}
		else{
			result = crossover(f1,f2);
		}
		
		countCalls +=1;
		countAffected +=2;
		
		context.getOffSpring().addAll(result);
		return result;
	}

	@Override
	public List<IIndividualTextEMF> crossover(IIndividualTextEMF f1, IIndividualTextEMF f2) {
		
		List<IIndividualTextEMF> result = new ArrayList<IIndividualTextEMF>();
		
		int parentSize=f1.getParentModelSize();
		int point=MathUtils.getRandomInt(parentSize-1);
		
		Chromosome mask = new Chromosome(parentSize);
		mask.set(0, point, true);
		mask.set(point+1,parentSize,false);
		
		Chromosome noMask = (Chromosome) mask.clone();
		noMask.negate();
		Chromosome aux1 = f1.getCopyOfGenes();
		Chromosome aux2 = f2.getCopyOfGenes();
		Chromosome aux3 = f1.getCopyOfGenes();
		Chromosome aux4 = f2.getCopyOfGenes();
		aux1.and(mask);
		aux2.and(noMask);
		aux1.or(aux2);
		
		result.add(new IndividualTextEMF(aux1,f1.getParent()));
		
		aux3.and(noMask);
		aux4.and(mask);
		aux3.or(aux4);
		
		result.add(new IndividualTextEMF(aux3,f1.getParent()));
		countCalls +=1;
		countAffected +=2;
		return result;
	}
}

