package usj.svit.approach.flimea.ldafitness;

import java.util.Arrays;
import java.util.List;

import jgibblda.Estimator;
import jgibblda.LDACmdOption;
import jgibblda.Model;
import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.fitness.IFitness;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.IndividualTextEMF;

public class FitnessLDA extends BaseOperation implements IFitness<IIndividualTextEMF>{

	double min,max;
	double factor = 1;
	
	
	LDACmdOption ldaOption;
	Estimator estimator;
	double [] similarity;
	double [] similarityQueryPerDocument;
	static double maxp=-1;
	static int index=-1;
	
	
	
	@Override
	public IPopulation<IIndividualTextEMF> assess(EAConfigTextEMF config) {
		
		ldaOption = new LDACmdOption();
		ldaOption.inf = true; 
		ldaOption.dir = "./"; 
		ldaOption.modelName = "TM"; 
		ldaOption.niters = 300;
		ldaOption.dfile="docs.dat";
		ldaOption.est=true;
		//ldaOption.estc=true;
		//ldaOption.savestep=100;
		ldaOption.K=300;
		ldaOption.alpha=1;
		ldaOption.beta=0.5;
		
		List<IIndividualTextEMF> individuals = config.getPopulation().getIndividuals();
		
		estimator = new Estimator();
		estimator.init(ldaOption, individuals);
		estimator.estimate();
		
		double [] similarity=computeQuerySimilarity(config.getQuery().getText(), estimator.trnModel);
		computeMaxProbability();
		System.out.println("\nDoc "+index+" has the maximum probability "+maxp);
		
		
		//calculate boundaries (max and min value)
		for(int i=0;i<similarity.length;i++)
		{
			assess(individuals.get(i), similarity[i]);
			countCalls -= 1;
		}
		countCalls += 1;
		return config.getPopulation();
	}
	
	 private double differencesSizes(String[] query, String[] individual) {
	        String[] sortedQuery = Arrays.copyOf(query, query.length); // O(n)
	        String[] sortedIndividual = Arrays.copyOf(individual, individual.length); // O(m)
	        Arrays.sort(sortedQuery); // O(n log n)
	        Arrays.sort(sortedIndividual); // O(m log m)

	        int queryIndex = 0;
	        int individualIndex = 0;

	        //LinkedList<String> diffs = new LinkedList<String>();  
	        int diffSize=0;
	        while (queryIndex < sortedQuery.length && individualIndex < sortedIndividual.length) { // O(n + m)
	            int compare = (int) Math.signum(sortedQuery[queryIndex].compareTo(sortedIndividual[individualIndex]));

	            switch(compare) {
	            case -1:
	                diffSize++;
	                queryIndex++;
	                break;
	            case 1:
	                diffSize++;
	                individualIndex++;
	                break;
	            default:
	                queryIndex++;
	                individualIndex++;
	            }
	        }
	        
	        diffSize += (sortedQuery.length - queryIndex);
	        diffSize += (sortedIndividual.length - individualIndex);
	        
	        double acertadas = factor*(query.length + individual.length - diffSize)/2;
	        
	    	double score = acertadas - diffSize ;
		
//	    	System.out.println("\t\tQuery: "+sortedQuery.length+" - Answer: "+sortedIndividual.length+" - Coinciden: "+(query.length + individual.length - diffSize)/2 +" - Score: "+score+" / "+max);
	    	
			return normalizeToZeroOne(score);
	    }
	 
	 private double normalizeToZeroOne(double value){
		 
		 return (value - min)/(max-min);
	 }

	@Override
	public void setBoundaries(EAConfigTextEMF config) {
		//the maximum score possible for the TC means matching the whole query and nothing else
		max = factor*config.getQuery().getText().length;
		
		//the minimum score possible for the TC means being the opposite to the oracle
		IndividualTextEMF individual = new IndividualTextEMF(config.getCurrentTestCaseAnswer().getCopyOfGenes(),config.getSearchSpace());
		individual.getGenes().negate();
		individual.updateTextsAfterRecombinations();
		
		min = -individual.getText().length;
		
	}

	@Override
	public IIndividualTextEMF assess(IIndividualTextEMF individual, EAConfigTextEMF config) {
		individual.setFitness(differencesSizes(config.getQuery().getText(), individual.getText()));
		countCalls += 1;
		countAffected += 1;
		return individual;
	}
	public IIndividualTextEMF assess(IIndividualTextEMF individual, Double value) {
		individual.setFitness(value);
		countCalls += 1;
		countAffected += 1;
		return individual;
	}
	
	public double [] computeQuerySimilarity(String [] queryTerms, Model model)
	{
	    
	    
	    int queryLength=queryTerms.length;
	    
	    //model.M = number of documents
		double [][] similarityWordPerDocument=new double[queryLength][model.M];
		similarityQueryPerDocument=new double [model.M];
		for(int i=0;i<model.M;i++)
		{
			similarityQueryPerDocument[i]=1.0;
		}
		
		
		for(int q=0;q<queryLength;q++)
		{
			String word=queryTerms[q];
			try {
				int wordId=model.data.localDict.word2id.get(word);
			
			
				//M= number of documents
				for(int d=0;d<model.M;d++)
				{
					//K= number of topics
					for(int t=0;t<model.K;t++)
					{
						if(wordId>=0)
						{
							//to avoid an exception in case that the query obtains terms that are not included in the documents.
							similarityWordPerDocument[q][d] += model.phi[t][wordId]*model.theta[d][t];
						}
						else
						{
							
						}
						
					}
				
				}
			
			}
			catch(NullPointerException e)
			{
				//in case the word of the query is not included in the documents
			}
		
		}
		for(int d=0;d<model.M;d++)
		{
			for(int q=0;q<queryLength;q++)
			{
				
				if(similarityWordPerDocument[q][d]>0) 
				{
				  similarityQueryPerDocument[d]*=similarityWordPerDocument[q][d];
				}
			}
		}
		
		
	 
	 return similarityQueryPerDocument;
	}
	
	public void computeMaxProbability()
	{
		maxp = similarityQueryPerDocument[0];
		index = 0;

		for (int i = 0; i < similarityQueryPerDocument.length; i++) 
		{
			if (maxp < similarityQueryPerDocument[i]) 
			{
				maxp = similarityQueryPerDocument[i];
				index = i;
			}
		}
		

	}
	
}
