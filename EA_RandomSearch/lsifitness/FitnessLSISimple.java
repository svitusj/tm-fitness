package usj.svit.approach.flimea.lsifitness;

import java.util.Arrays;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.fitness.IFitness;
import usj.svit.approach.flimea.core.operator.BaseOperation;
import usj.svit.approach.flimea.core.population.IPopulation;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.IndividualTextEMF;

public class FitnessLSISimple extends BaseOperation implements IFitness<IIndividualTextEMF>{

	double min,max;
	double factor = 1;
	
	@Override
	public IPopulation<IIndividualTextEMF> assess(EAConfigTextEMF config) {
		
		//calculate boundaries (max and min value)
		for(IIndividualTextEMF individual : config.getPopulation().getIndividuals()){
			
			assess(individual,config);//.setFitness(differencesSizes(config.getQuery().getText(), individual.getText()));
			countCalls -= 1;
		}
		countCalls += 1;
		return config.getPopulation();
	}
	
	 private double differencesSizes(String[] query, String[] individual) {
	        String[] sortedQuery = Arrays.copyOf(query, query.length); // O(n)
	        String[] sortedIndividual = Arrays.copyOf(individual, individual.length); // O(m)
	        Arrays.sort(sortedQuery); // O(n log n)
	        Arrays.sort(sortedIndividual); // O(m log m)

	        int queryIndex = 0;
	        int individualIndex = 0;

	        //LinkedList<String> diffs = new LinkedList<String>();  
	        int diffSize=0;
	        while (queryIndex < sortedQuery.length && individualIndex < sortedIndividual.length) { // O(n + m)
	            int compare = (int) Math.signum(sortedQuery[queryIndex].compareTo(sortedIndividual[individualIndex]));

	            switch(compare) {
	            case -1:
	                diffSize++;
	                queryIndex++;
	                break;
	            case 1:
	                diffSize++;
	                individualIndex++;
	                break;
	            default:
	                queryIndex++;
	                individualIndex++;
	            }
	        }
	        
	        diffSize += (sortedQuery.length - queryIndex);
	        diffSize += (sortedIndividual.length - individualIndex);
	        
	        double acertadas = factor*(query.length + individual.length - diffSize)/2;
	        
	    	double score = acertadas - diffSize ;
		
//	    	System.out.println("\t\tQuery: "+sortedQuery.length+" - Answer: "+sortedIndividual.length+" - Coinciden: "+(query.length + individual.length - diffSize)/2 +" - Score: "+score+" / "+max);
	    	
			return normalizeToZeroOne(score);
	    }
	 
	 private double normalizeToZeroOne(double value){
		 
		 return (value - min)/(max-min);
	 }

	@Override
	public void setBoundaries(EAConfigTextEMF config) {
		//the maximum score possible for the TC means matching the whole query and nothing else
		max = factor*config.getQuery().getText().length;
		
		//the minimum score possible for the TC means being the opposite to the oracle
		IndividualTextEMF individual = new IndividualTextEMF(config.getCurrentTestCaseAnswer().getCopyOfGenes(),config.getSearchSpace());
		individual.getGenes().negate();
		individual.updateTextsAfterRecombinations();
		
		min = -individual.getText().length;
		
	}

	@Override
	public IIndividualTextEMF assess(IIndividualTextEMF individual, EAConfigTextEMF config) {
		individual.setFitness(differencesSizes(config.getQuery().getText(), individual.getText()));
		countCalls += 1;
		countAffected += 1;
		return individual;
	}
}
