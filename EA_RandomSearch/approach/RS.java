package usj.svit.experiment.ist20_tm.approaches;

import usj.svit.approach.flimea.core.config.EAConfigTextEMF;
import usj.svit.approach.flimea.core.ea.BaseEATextEMF;
import usj.svit.approach.flimea.core.pre.IPreProcessor;
import usj.svit.architecture.individual.IIndividualEMF;
import usj.svit.architecture.individual.IIndividualTextEMF;
import usj.svit.architecture.individual.impl.IndividualTextEMF;
import usj.svit.architecture.oracle.TestCaseTextEMF;
import usj.svit.architecture.results.EMFResultSingle;
import usj.svit.architecture.results.IEMFResult;

public class RS extends BaseEATextEMF {

	static int id_counter=0;
	int id;
	
	public RS(EAConfigTextEMF config) {
		super(config);
		id = id_counter;
		id_counter++;
	}

	@Override
	public IEMFResult<IIndividualEMF> run(TestCaseTextEMF testCase) {
		

		this.config.setCurrentTestCase(testCase);
		
		this.config.getPopulation().setPopulation(config.getPopulator().generateInitialPopulation(config));
		
		for (IPreProcessor<IIndividualTextEMF> preprocessor : config.getPreprocessors()) {
			preprocessor.process(config);
		}
		
		this.config.getFitness().setBoundaries(this.config);
		
		this.loop();
		System.out.println(this.getClass().getSimpleName()+" - TC: "+testCase.getID()+" - "+config.getPopulation().get(0).getFitness());
		return new EMFResultSingle<IIndividualEMF>(config.getPopulation().get(0));
		
	}
	
	@Override
	public void loop() {

//		long t0 = System.currentTimeMillis();
		
		IndividualTextEMF best = null;
		IndividualTextEMF candidate = null;
		
		while (true) {

			for (IIndividualTextEMF element : config.getPopulation().getIndividuals()) {
				element.updateTextsAfterRecombinations();
			}
			
			config.getFitness().assess(config);
			
			//config.getPopulation().sortPopulationDescendant();
			
			candidate = (IndividualTextEMF) config.getPopulation().getBestIndividual();
			
			if(best == null || best.getFitness() < candidate.getFitness())
				best = candidate;
			
//			System.out.println("Gen: "+config.getCurrentGeneration() +" - "+best.getFitness()  +" - "+candidate.getFitness() +  " - "+(System.currentTimeMillis()-t0));
//			t0 = System.currentTimeMillis();
			
			if (config.stopConditionMeet()) {
				config.getPopulation().setPopulation(best);
				//System.out.println("Gen: "+config.getCurrentGeneration() +" - "+config.getPopulation().get(0).getFitness());
				return;
			}
			
			config.getPopulation().getIndividuals().clear();
			for(int i=0;i<config.MAX_POPULATION;i++){
				IIndividualTextEMF individual = config.getPopulator().generateIndividual(config);
				config.getPopulation().getIndividuals().add(individual);
			}
			
			config.increaseCurrentGeneration();
		}
	}

	@Override
	public String getID() {
		return "RS_"+id;
	}

}

